#!/bin/bash
echo "+++"
sudo -s bash << ROOT
cp user_login_hook.so /usr/lib/postgresql/11/lib
cp user_login_hook--1.0.sql /usr/share/postgresql/11/extension/
cp user_login_hook--1.0.control /usr/share/postgresql/11/extension/
cp user_login_hook.control /usr/share/postgresql/11/extension/

sudo -u postgres bash << PGSQL
psql -c "DROP EXTENSION IF EXISTS user_login_hook; CREATE EXTENSION user_login_hook;"
psql -c "SELECT 1;"

tail /var/log/postgresql/postgresql-11-main.log 
PGSQL
ROOT

