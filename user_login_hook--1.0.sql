create or replace function getUserLoginHookVersion()
    returns text 
    immutable leakproof
    as 'user_login_hook.so', 'get_user_login_hook_version'
    language C
    security definer;
    
comment on function getUserLoginHookVersion() is 'Returns the version of this database''s user_login_hook database extension';

