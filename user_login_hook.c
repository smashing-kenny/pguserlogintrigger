/**
Derevo vipolnenie funkcij
InitPostgres (postinit.c)
	PerformAuthentication (postinit.c)
		ClientAuthentication (auth.c)
			ClientAuthentication_hook 
**/

#include "postgres.h"

#include "executor/spi.h"
#include "miscadmin.h"

#include "commands/dbcommands.h"
#include "access/parallel.h"
#include "access/xact.h"
#include "access/transam.h"
#include "catalog/namespace.h"
#include "utils/array.h"
#include "catalog/pg_type.h"
#include "catalog/pg_namespace.h"
//pg_namespace_d.h
#include "utils/syscache.h"
#include "utils/builtins.h"

#include "commands/seclabel.h"
#include "libpq/auth.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "utils/guc.h"

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

static ClientAuthentication_hook_type original_client_auth_hook = NULL;
static char *pguserlogin_hook_value = NULL;

static bool pguserlogin_hook_check_hook(char **newvalue, void **extra, GucSource source){
	elog(LOG,"UserLoginHook check hook %s",*newvalue);

	if(*newvalue){
		/* Need a modifiable copy of string */
		char *rawstring = pstrdup(*newvalue);

		if(!SearchSysCacheExists3(
				PROCNAMEARGSNSP,
				CStringGetDatum(rawstring),
				PointerGetDatum(buildoidvector(NULL, 0)),
				ObjectIdGetDatum(PG_PUBLIC_NAMESPACE))){

			GUC_check_errdetail("triger \"%s\" function does not exist", rawstring);
			pfree(rawstring);
			return false;
		}

		pfree(rawstring);
	}

	return true;
}

static void pguserlogin_hook_assign_hook(const char *newvalue, void *extra){
	elog(LOG,"UserLoginHook assign hook newvalue %s",newvalue);

}

static void pguserlogin_login_hook(Port *port, int status){
	elog(LOG,
		"UserLoginHook hook MyProcPid=%d, MyDatabaseId=%d, IsBackgroundWorker=%d",
		MyProcPid, MyDatabaseId, IsBackgroundWorker);

	if (original_client_auth_hook)
		(*original_client_auth_hook) (port, status);

	/*
	 * If the authentication fails, no need to go further
	 */
	if(status != STATUS_OK)
		return;

	elog(LOG,"UserLoginHook call hook when user login");

	const char *hook = GetConfigOption("pguserlogin.hook", false, false);
	elog(LOG,"UserLoginHook user:(%d %s) hook:(%s %s)",
			GetUserId(),
			GetUserNameFromId(GetUserId(), true),
			hook,
			pguserlogin_hook_value);

	if(!hook){
		return;
	}

	elog(LOG,"UserLoginHook pguserlogin_hook_value:%s",hook);
	if(SearchSysCacheExists3(
		PROCNAMEARGSNSP,
		CStringGetDatum(hook),
		PointerGetDatum(buildoidvector(NULL, 0)),
		ObjectIdGetDatum(PG_PUBLIC_NAMESPACE))){

		SPI_connect();
		char query[1024];
		sprintf(query, "select %s();", hook);
		elog(LOG,"UserLoginHook query:%s", query);
		SPI_execute(query, false, 1);
		SPI_finish();
	}
}


void _PG_init(void){
	elog(LOG,"UserLoginHook _PG_init()");

	/************************************************************
	 * Define PL/Tcl's custom GUCs
	 ************************************************************/
	DefineCustomStringVariable(
		/*name*/"pguserlogin.hook",
		/*short desc*/gettext_noop("pgUserLogin trigger function which called when user login"),
		/*long*/NULL,
		/*value*/ &pguserlogin_hook_value,
		/*boot value*/NULL,
		/*context*/PGC_USERSET,
		/*flags*/GUC_IS_NAME,
		/*check_hook*/pguserlogin_hook_check_hook,
		/*assign_hook*/ pguserlogin_hook_assign_hook,
		/*show_hook*/NULL);

    original_client_auth_hook = ClientAuthentication_hook;
    ClientAuthentication_hook = pguserlogin_login_hook;
}

void _PG_fini(void){
	elog(LOG,"UserLoginHook _PG_fini()");
	ClientAuthentication_hook = original_client_auth_hook;
}
